require ipmiComm, 4.2.0-rc.1

require iocStats, 3.1.16
require recsync, 1.3.0-9705e52
require caPutLog, b544f92
require autosave, 5.10.0

epicsEnvSet("TOP",              "$(E3_CMD_TOP)/..")

epicsEnvSet("DEVICE_IP",        "172.30.4.208")
epicsEnvSet("P",                "$(P=LabS:)")  # Sys-Sub:
epicsEnvSet("DIS",              "Ctrl")
epicsEnvSet("SYSIDX",           "1")
epicsEnvSet("R",                "$(R=$(DIS)-MTCA-$(SYSIDX)00:)")
epicsEnvSet("PREFIX",           "$(P)$(R)")

epicsEnvSet("ENGINEER",         "anderslindh <anders.lindholsson@ess.eu>")
epicsEnvSet("LOCATION",         "ALO test crate")
epicsEnvSet("IOCNAME",          "$(PREFIX)")

iocshLoad("$(ipmiComm_DIR)mtca9u_std.iocsh",    "SESS=$(PREFIX), INET=$(DEVICE_IP), PREF=$(P)$(DIS)-, SYSIDX=$(SYSIDX), LOCT=$(LOCATION)")
iocshLoad("$(ipmiComm_DIR)sis8300ku.iocsh",     "SESS=$(PREFIX), FRUID=5, PREF=$(P), ID=$(DIS)-SIS8300, UNIT=$(SYSIDX)01")
iocshLoad("$(ipmiComm_DIR)ifc14xx.iocsh",       "SESS=$(PREFIX), FRUID=6, PREF=$(P), ID=$(DIS)-IFC1420, UNIT=$(SYSIDX)01")
iocshLoad("$(ipmiComm_DIR)am90x-x1x.iocsh",     "SESS=$(PREFIX), FRUID=7, PREF=$(P), ID=$(DIS)-CPU, UNIT=$(SYSIDX)01")
iocshLoad("$(ipmiComm_DIR)evr300.iocsh",        "SESS=$(PREFIX), FRUID=8, PREF=$(P), ID=$(DIS)-EVR, UNIT=$(SYSIDX)01")

#loadIocsh("iocStats.iocsh",    "IOCNAME=$(IOCNAME)")
#loadIocsh("recsync.iocsh",     "IOCNAME=$(IOCNAME)")

iocInit()

dbl > "PVs.list"
