require ipmicomm, 4.2.5
require iocstats, 3.1.16
require autosave, 5.10.2

epicsEnvSet("TOP",              "$(E3_CMD_TOP)/..")

epicsEnvSet("DEVICE_IP",        "$(DEVICE_IP=)")
epicsEnvSet("P",                "$(P=LabS:)")  # Sys-Sub:
epicsEnvSet("DIS",              "Ctrl")
epicsEnvSet("SYSIDX",           "1")
epicsEnvSet("R",                "$(R=$(DIS)-MTCA-$(SYSIDX)00:)")
epicsEnvSet("PREFIX",           "$(P)$(R)")

epicsEnvSet("ENGINEER",         "henriquesilva <henrique.silva@ess.eu>")
epicsEnvSet("LOCATION",         "MTCA Crate under test")
epicsEnvSet("IOCNAME",          "$(PREFIX)")

## Only for cellmode
#epicsEnvSet(EPICS_DRIVER_PATH, "$(PWD)/cellMods:$(EPICS_DRIVER_PATH)")

### DBs for specific crate configuration

## Common infrastructure (MCH+CPU+EVR)
iocshLoad("$(ipmicomm_DIR)mtca9u_std.iocsh",    "SESS=$(PREFIX), INET=$(DEVICE_IP), PREF=$(P)$(DIS)-, SYSIDX=$(SYSIDX), LOCT=$(LOCATION)")
iocshLoad("$(ipmicomm_DIR)evr300.iocsh",        "SESS=$(PREFIX), FRUID=5, PREF=$(P), ID=$(DIS)-EVR, UNIT=$(SYSIDX)01")
#iocshLoad("$(ipmicomm_DIR)amg6x-msd.iocsh",     "SESS=$(PREFIX), FRUID=6, PREF=$(P), ID=$(DIS)-CPU, UNIT=$(SYSIDX)01")
iocshLoad("$(ipmicomm_DIR)am90x-x1x.iocsh",     "SESS=$(PREFIX), FRUID=6, PREF=$(P), ID=$(DIS)-CPU, UNIT=$(SYSIDX)01")

## Application specific AMCs (Struck, IOxOs, etc)
#iocshLoad("$(ipmicomm_DIR)sis8300ku.iocsh",     "SESS=$(PREFIX), FRUID=7, PREF=$(P), ID=$(DIS)-SIS8300, UNIT=$(SYSIDX)01")
#iocshLoad("$(ipmicomm_DIR)sis8300ku.iocsh",     "SESS=$(PREFIX), FRUID=8, PREF=$(P), ID=$(DIS)-SIS8300, UNIT=$(SYSIDX)02")
#iocshLoad("$(ipmicomm_DIR)sis8300ku.iocsh",     "SESS=$(PREFIX), FRUID=9, PREF=$(P), ID=$(DIS)-SIS8300, UNIT=$(SYSIDX)03")
#iocshLoad("$(ipmicomm_DIR)sis8300ku.iocsh",     "SESS=$(PREFIX), FRUID=10, PREF=$(P), ID=$(DIS)-SIS8300, UNIT=$(SYSIDX)04")
#iocshLoad("$(ipmicomm_DIR)sis8300ku.iocsh",     "SESS=$(PREFIX), FRUID=11, PREF=$(P), ID=$(DIS)-SIS8300, UNIT=$(SYSIDX)05")
#iocshLoad("$(ipmicomm_DIR)sis8300ku.iocsh",     "SESS=$(PREFIX), FRUID=12, PREF=$(P), ID=$(DIS)-SIS8300, UNIT=$(SYSIDX)06")
#iocshLoad("$(ipmicomm_DIR)sis8300ku.iocsh",     "SESS=$(PREFIX), FRUID=13, PREF=$(P), ID=$(DIS)-SIS8300, UNIT=$(SYSIDX)07")
#iocshLoad("$(ipmicomm_DIR)sis8300ku.iocsh",     "SESS=$(PREFIX), FRUID=14, PREF=$(P), ID=$(DIS)-SIS8300, UNIT=$(SYSIDX)08")
#iocshLoad("$(ipmicomm_DIR)sis8300ku.iocsh",     "SESS=$(PREFIX), FRUID=15, PREF=$(P), ID=$(DIS)-SIS8300, UNIT=$(SYSIDX)09")
#iocshLoad("$(ipmicomm_DIR)sis8300ku.iocsh",     "SESS=$(PREFIX), FRUID=16, PREF=$(P), ID=$(DIS)-SIS8300, UNIT=$(SYSIDX)10")

#iocshLoad("$(ipmicomm_DIR)ifc14xx.iocsh",       "SESS=$(PREFIX), FRUID=6, PREF=$(P), ID=$(DIS)-IFC1420, UNIT=$(SYSIDX)01")

#loadIocsh("iocStats.iocsh",    "IOCNAME=$(IOCNAME)")

iocInit()
