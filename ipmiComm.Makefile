where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



#
USR_CFLAGS   += -Wno-unused-but-set-variable
USR_CPPFLAGS += -Wno-unused-but-set-variable
#

APP:=.
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

USR_INCLUDES += -I$(where_am_I)$(APPSRC)


TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.template)
TEMPLATES += $(wildcard ../template/*_ess.template)


DBDS      += $(APPSRC)/ipmiComm.dbd
#DBDS      += $(APPSRC)/drvMchServerPc.dbd
#DBDS      += $(APPSRC)/drvMchPicmg.dbd


SOURCES   += $(APPSRC)/drvMch.c
SOURCES   += $(APPSRC)/devMch.c
SOURCES   += $(APPSRC)/drvMchMsg.c
SOURCES   += $(APPSRC)/ipmiMsg.c
SOURCES   += $(APPSRC)/ipmiDef.c
SOURCES   += $(APPSRC)/picmgDef.c
SOURCES   += $(APPSRC)/drvMchPicmg.c
SOURCES   += $(APPSRC)/drvMchServerPc.c
SOURCES   += $(APPSRC)/subIpmiComm.c

SCRIPTS += $(wildcard ../iocsh/*.iocsh)

# Order matters
SUBS += ../template/fru_extended_ess.substitutions
SUBS += $(wildcard ../template/others_*_ess.substitutions)
SUBS += $(wildcard ../template/ifc14*_ess.substitutions)
SUBS += $(wildcard ../template/sis8300*_ess.substitutions)
SUBS += $(wildcard ../template/mtcaevr*_ess.substitutions)
SUBS += $(wildcard ../template/mtcaevm*_ess.substitutions)
SUBS += $(wildcard ../template/cpu_*_ess.substitutions)
SUBS += $(wildcard ../template/mch_*_ess.substitutions)
SUBS += $(wildcard ../template/cu_*_ess.substitutions)
SUBS += $(wildcard ../template/pm_*_ess.substitutions)


USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)
USR_DBFLAGS += -I $(where_am_I)/../template


.PHONY: vlibs
vlibs:
