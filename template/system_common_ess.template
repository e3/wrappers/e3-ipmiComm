record(mbbi, "$(P)$(R)Type_Raw") {
 field(DESC, "Device type")
 field(DTYP, "MCHsensor")
 field(SCAN, "I/O Intr")
 field(INP, "#B0 C0 N0 @$(link)+mch")
 field(ZRST, "Unknown")
 field(ZRVL, "0")
 field(ZRSV, "MAJOR")
 field(ONST, "MicroTCA")
 field(ONVL, "1")
 field(ONSV, "NO_ALARM")
 field(TWST, "MicroTCA")
 field(TWVL, "2")
 field(TWSV, "NO_ALARM")
 field(THST, "Supermicro")
 field(THVL, "3")
 field(THSV, "NO_ALARM")
 field(FRST, "ATCA")
 field(FRVL, "4")
 field(FRSV, "NO_ALARM")
 field(FVVL, "5")
 field(FVST, "ATCA")
 field(FVSV, "NO_ALARM")
 field(SXVL, "6")
 field(SXST, "Advantech")
 field(SXSV, "NO_ALARM")
 field(PINI, "YES")
}

record(stringin, "$(P)$(R)Loc") {
  field(PINI, "YES")
  field(VAL, "$(location)")
  info(autosaveFields,"VAL")
}

# Control debug message verbosity
record(mbbo, "$(P)$(R)Dbg") {
 field(DTYP, "MCHsensor")
 field(OMSL, "supervisory")
 field(ZRVL, "0")
 field(ZRST, "Off")
 field(ONVL, "1")
 field(ONST, "Low")
 field(TWVL, "2")
 field(TWST, "Med")
 field(THVL, "3")
 field(THST, "High")
 field(OUT,  "#B0 C0 N0 @$(link)+dbg")
 field(VAL,  "0")
 field(PINI, "YES")
}

record(bi, "$(P)$(R)OnlnStat") {
 field(DESC, "MCH online status")
 field(DTYP, "MCHsensor")
 field(SCAN, "I/O Intr")
 field(INP, "#B0 C0 N0 @$(link)+stat")
 field(ZNAM, "Offline")
 field(ZSV, "MAJOR")
 field(ONAM, "Online")
 field(OSV, "NO_ALARM")
 field(PINI, "YES")
}

record(mbbi, "$(P)$(R)Init") {
 field(DESC, "MCH comm initialized")
 field(DTYP, "MCHsensor")
 field(SCAN, "I/O Intr")
 field(INP, "#B0 C0 N0 @$(link)+init")
 field(ZRST, "Not initialized")
 field(ZRVL, "0")
 field(ZRSV, "MAJOR")
 field(ONST, "Initializing...")
 field(ONVL, "1")
 field(ONSV, "MINOR")
 field(TWST, "Initialized")
 field(TWVL, "2")
 field(TWSV, "NO_ALARM")
 field(THST, "Initialize failed")
 field(THVL, "3")
 field(THSV, "MAJOR")
 field(PINI, "YES")
}

record(bo, "$(P)$(R)InitBYP") {
 field(DESC, "Override init for testing")
 field(DTYP, "MCHsensor")
 field(OMSL, "supervisory")
 field(OUT, "#B0 C0 N0 @$(link)+init")
 field(ZNAM, "Not initialized")
 field(ZSV, "MAJOR")
 field(ONAM, "Initialized")
 field(OSV, "NO_ALARM")
 field(PINI, "NO")
}

record(bo, "$(P)$(R)Connect") {
 field(DESC, "Enable comm with MCH")
 field(DTYP, "MCHsensor")
 field(OMSL, "supervisory")
 field(OUT,  "#B0 C0 N0 @$(link)+sess")
 field(ZNAM, "Disconnect")
 field(ZSV,  "MAJOR")
 field(ONAM, "Connect")
 field(OSV,  "NO_ALARM")
 field(VAL, "1")
 field(RVAL, "1")
 field(PINI, "YES")
 info(autosaveFields,"VAL RVAL")
}

# Control sensor scan period [seconds]
# Default is 10 seconds
# Added this because PAL systems had
# trouble at that rate
# Must match SENSOR_SCAN_PERIODS array
# definition in devMch.c
record(mbbo, "$(P)$(R)Sensor_Scan_Period") {
 field(DTYP, "MCHsensor")
 field(OMSL, "supervisory")
 field(ZRVL, "0")
 field(ZRST, "5 seconds")
 field(ONVL, "1")
 field(ONST, "10 seconds")
 field(TWVL, "2")
 field(TWST, "20 seconds")
 field(THVL, "3")
 field(THST, "30 seconds")
 field(FRVL, "4")
 field(FRST, "60 seconds")
 field(OUT,  "#B0 C0 N0 @$(link)+scan")
 field(VAL,  "1")
 field(RVAL, "1")
 field(PINI, "YES")
 info(autosaveFields,"VAL RVAL")
}
